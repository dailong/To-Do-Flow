//
//  CellConfigurable.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

protocol CellConfigurable {
    func configure(withViewModel viewModel: RowViewModel)
    func delegate(_ viewController: UIViewController)
}


protocol CellSelectable {
}
//protocol CellPressible {
//    func cellPressed()
//}
