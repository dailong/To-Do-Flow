//
//  DetailRemindMeOnDateCell.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

protocol DetailRemindMeOnDateDelegate: class {
    func switchButtonRemindOnDateValueChanged(_ sender: UISwitch)
}

class DetailRemindMeOnDateCell: UITableViewCell {

    @IBOutlet weak var switchButton: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
    
    weak var delegate: DetailRemindMeOnDateDelegate?

    static var identifier: String {
        return String(describing: self)
    }
    
    
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        delegate?.switchButtonRemindOnDateValueChanged(sender)
    }
    
}

extension DetailRemindMeOnDateCell: CellConfigurable {
    func configure(withViewModel viewModel: RowViewModel) {
        guard let viewModel = viewModel as? DetailRemidOnDateCellViewModel else { fatalError("Unexpected type") }
        
        switchButton.isOn = viewModel.isOn
    }
    
    func delegate(_ viewController: UIViewController) {
        if let viewVC = viewController as? DetailsTableVC {

            self.delegate = viewVC
        }
    }
}
