//
//  DetailTitleCell.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit


class DetailTitleCell: UITableViewCell {

    @IBOutlet weak var titleTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleTextView.isScrollEnabled = false
        titleTextView.backgroundColor = .clear
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
}

extension DetailTitleCell:CellConfigurable {
    func configure(withViewModel viewModel: RowViewModel) {
        guard let viewModel = viewModel as? DetailTitleCellViewModel else { fatalError("Unexpected type") }
        titleTextView.text = viewModel.text
    }
    
    func delegate(_ viewController: UIViewController) {
        if let viewVC = viewController as? DetailsTableVC {
            titleTextView.delegate = viewVC
        }
    }
}
