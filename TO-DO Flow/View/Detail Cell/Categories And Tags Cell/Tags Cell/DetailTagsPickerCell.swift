//
//  DetailTagsPickerCell.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/24/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

class DetailTagsPickerCell: UITableViewCell {

    @IBOutlet weak var tagsPickerView: HorizontalScrollerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }

}
extension DetailTagsPickerCell: CellConfigurable {
    func configure(withViewModel viewModel: RowViewModel) {
     //   guard let _ = viewModel as? DetailTagsPickerViewModel else { fatalError("Unexpected type") }
    }
    
    func delegate(_ viewController: UIViewController) {
        guard let detailsTableVC = viewController as? DetailsTableVC else { return }
        guard let viewModel = detailsTableVC.viewModels[3] as? CategoriesAndTagsViewModel else {return}
        
        guard let rowViewModel = viewModel.viewModel(for: 2) as? DetailTagsPickerViewModel else {return}
        tagsPickerView.delegate = detailsTableVC//rowViewModel
        tagsPickerView.dataSource = rowViewModel
        tagsPickerView.reload()
    }
}
