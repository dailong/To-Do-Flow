//
//  DetailNotesCell.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit


class DetailNotesCell: UITableViewCell {

    @IBOutlet weak var notesTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        notesTextView.isScrollEnabled = false
        notesTextView.backgroundColor = .clear
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }

}

extension DetailNotesCell: CellConfigurable {

    
    func configure(withViewModel viewModel: RowViewModel) {
        guard let viewModel = viewModel as? DetailNotesCellViewModel else { fatalError("Unexpected type") }
        notesTextView.text = viewModel.text
    }
    
    func delegate(_ viewController: UIViewController) {
        if let viewVC = viewController as? DetailsTableVC {
            notesTextView.delegate = viewVC
        }
    }
}

