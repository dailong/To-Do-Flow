//
//  String.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit


extension String {

    
    func toObject(_ cell: inout UITableViewCell) {
        switch self {
        case "DetailTitleCell":
            let _ = cell as? DetailNotesCell
        case "DetailRemindMeOnDateCell":
            let _ = cell as? DetailRemindMeOnDateCell
        case "DetailAlarmCell":
            let _ = cell as? DetailAlarmCell
        case "DetailDatePickerCell":
            let _ = cell as? DetailDatePickerCell
        case "DetailFrequencyCell":
            let _ = cell as? DetailFrequencyCell
        case "DetailRemindMeOnLocationCell":
            let _ = cell as? DetailRemindMeOnLocationCell
        case "DetailLocationPickerCell":
            let _ = cell as? DetailLocationPickerCell
        case "DetailCategoriesCell":
            let _ = cell as? DetailCategoriesCell
        case "DetailTagsCell":
            let _ = cell as? DetailTagsCell
        case "DetailNotesCell":
            let _ = cell as? DetailNotesCell
        default:
            fatalError("Unexpected \(self)")
        }
    }
}
