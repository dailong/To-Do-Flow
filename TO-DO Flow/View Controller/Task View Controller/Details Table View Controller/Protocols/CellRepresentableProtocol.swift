//
//  CellRepresentableProtocol.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

protocol CellRepresentable {
    var cellIndexPath: IndexPath? {get set}
    var hasInlineCell: Bool {get}
    
    mutating func displayCellInline(_ tableView: UITableView, at indexPath: IndexPath)
}
