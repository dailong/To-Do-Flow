//
//  SectionRepresentable.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

protocol SectionRepresentable {
    var numberOfRows: Int {get}
    var titleForHeader: String? {get}
    
    func cellIdentifier(for index: Int) -> String
    func viewModel(for index: Int) -> RowViewModel
}
