//
//  DetailsTableVC.swift
//  TO-DO Flow
//
//  Created by Dai Long on 7/25/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import CoreData
import EventKit


class DetailsTableVC: UITableViewController {
    
    var viewModels: [SectionRepresentable?] = []
    
  //  var horizontalScrollerView = HorizontalScrollerView()
    
    // MARK: - View life cycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // fetchTags()
        setupView()
      //  hideKeyboardWhenTappedAround()
    }
    
    deinit {
        print("Details Was deinited")
        self.viewModels.removeAll()
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.removeObserver(self,
                                          name: Notification.Name.NSManagedObjectContextObjectsDidChange,
                                          object: nil)
    }
    
    private func setupView() {
        title = "Details"
        setupTableView()
     //   setupHorizontalScrollerView()
    }
    
    // MARK: - Prepare

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard let identifier = segue.identifier else {return}
//
//        switch (identifier) {
//        case Segue.categorySegue:
//            guard let categoriesVC = segue.destination as? CategoriesVC else {return}
//
//            // Configure categoriesVC
//            categoriesVC.task = task
//
//        case Segue.showRepeatVC:
//            guard let repeatVC = segue.destination as? RepeatVC else { return }
//
//            let cell = sender as! UITableViewCell
//
//            // Configure repeatVC
//            repeatVC.displayedFrequency = cell.detailTextLabel?.text
//
//        default:
//            break
//        }
//    }
    
    // MARK: - UnwindSegueToDetailsVC
    @IBAction func unwindToDetailsVC (segue: UIStoryboardSegue) {
//        let repeatVC = segue.source as! RepeatVC
//
//        var frequencyCell: UITableViewCell? = nil
//
//        // The frequency cell is at row 3 when the date picker is shown and at row 2, otherwise
//        if self.hasInlineDatePicker {
//            frequencyCell = self.tableView.cellForRow(at: IndexPath(row: 3, section: 1))
//        } else {
//            frequencyCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 1))
//        }
//        // Display the frequency value
//        frequencyCell?.detailTextLabel?.text = repeatVC.displayedFrequency
    }

    // MARKL - Actions
    
    @IBAction func doneButtonWasPressed(_ sender: UIBarButtonItem) {

        guard let viewModel = self.viewModels[1] as? RemindOnDateViewModel else {return}
        
        if viewModel.task.hasAlarm {
            // Start Date
            let startDate = viewModel.displayDate

            // Frequency
            var frequencyCell: UITableViewCell? = nil
            if viewModel.hasInlineCell {
                frequencyCell = self.tableView.cellForRow(at: IndexPath(row: 3, section: 1))
            } else {
                frequencyCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 1))
            }
            let frequency = frequencyCell?.detailTextLabel!.text

            // Create Reminder
            var reminder = TimedReminder(title: viewModel.task.title!,
                                         startDate: startDate,
                                         frequency: frequency!)
            
            if let taskReminder = viewModel.taskReminder {
                // Modify Reminder
                TimedReminderStore.shared.updateReminder(taskReminder, reminder)
            }
            else {

                // Create Reminder
                TimedReminderStore.shared.createReminder(&reminder)
                viewModel.task.calendarIdentifier = reminder.calendarIdentifier
            }
        }
        else {
            // Remove Reminder
            if let reminder = viewModel.taskReminder {
                TimedReminderStore.shared.remove(reminder)
            }
        }
        
        // Dismiss Details View Controller
        dismiss(animated: true, completion: nil)
    }
    
}

/* ------------------------------- */
extension DetailsTableVC {
    
    private func setupTableView() {
        tableView.alwaysBounceHorizontal = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(DetailTitleCell.nib,
                           forCellReuseIdentifier: DetailTitleCell.identifier)

        tableView.register(DetailRemindMeOnDateCell.nib,
                           forCellReuseIdentifier: DetailRemindMeOnDateCell.identifier)
        tableView.register(DetailAlarmCell.nib,
                           forCellReuseIdentifier: DetailAlarmCell.identifier)
        tableView.register(DetailDatePickerCell.nib,
                           forCellReuseIdentifier: DetailDatePickerCell.identifier)
        tableView.register(DetailFrequencyCell.nib,
                           forCellReuseIdentifier: DetailFrequencyCell.identifier)
        
        tableView.register(DetailRemindMeOnLocationCell.nib,
                           forCellReuseIdentifier: DetailRemindMeOnLocationCell.identifier)
        tableView.register(DetailLocationPickerCell.nib,
                           forCellReuseIdentifier: DetailLocationPickerCell.identifier)
        
        tableView.register(DetailCategoriesCell.nib,
                           forCellReuseIdentifier: DetailCategoriesCell.identifier)
        tableView.register(DetailTagsCell.nib,
                           forCellReuseIdentifier: DetailTagsCell.identifier)
        tableView.register(DetailTagsPickerCell.nib,
                           forCellReuseIdentifier: DetailTagsPickerCell.identifier)

        tableView.register(DetailNotesCell.nib,
                           forCellReuseIdentifier: DetailNotesCell.identifier)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int { return 5 }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let viewModel = self.viewModels[section] else { return nil }
        return viewModel.titleForHeader
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 1
        if let viewModel = viewModels[section] {
            numberOfRows = viewModel.numberOfRows
        }
        return numberOfRows
    }
    

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = self.viewModels[indexPath.section] else { return UITableViewCell() }
        
        let identifier = viewModel.cellIdentifier(for: indexPath.row)
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier) else { return UITableViewCell() }
        
        let rowViewModel = viewModel.viewModel(for: indexPath.row)
        
        if let cell = cell as? CellConfigurable {
            // Configure
            cell.configure(withViewModel: rowViewModel)
            cell.delegate(self)
        }
        return cell
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = self.viewModels[indexPath.section] else { return }

        let rowViewModel = viewModel.viewModel(for: indexPath.row)

        guard let _ = rowViewModel as? CellSelectable else {return}
        if let rowViewModel = rowViewModel as? ViewModelPressiable {
            rowViewModel.cellPressed(tableView, atIndexPath: indexPath, withViewModel: viewModel)
        }
    }
}
