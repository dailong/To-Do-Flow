//
//  DetailsTableVC+DelegateCell.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/24/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import Foundation

extension DetailsTableVC: DetailRemindAtLocationCellDelegate {
    
    func switchButtonValueChanged(_ sender: UISwitch) {
        // Check Permission Acess Reminder
        
        guard let indexPath = tableView.indexPath(for: sender) else { return }
        guard let viewModel = self.viewModels[indexPath.section] as? RemindAtLocationViewModel else {return}
        guard let rowViewModel = viewModel.viewModel(for: indexPath.row) as? DetailRemindAtLocationCellViewModel else { return }
        
        
        viewModel.task.hasLocationReminder = sender.isOn
        
        rowViewModel.cellPressed(tableView, atIndexPath: indexPath, withViewModel: viewModel)
    }
}


extension DetailsTableVC: DetailRemindMeOnDateDelegate {
    func switchButtonRemindOnDateValueChanged(_ sender: UISwitch) {
        
        guard let indexPath = tableView.indexPath(for: sender) else { return }
        guard let viewModel = self.viewModels[indexPath.section] as? RemindOnDateViewModel else {return}
        guard let rowViewModel = viewModel.viewModel(for: indexPath.row) as? DetailRemidOnDateCellViewModel else { return }
        
        viewModel.task.hasAlarm = sender.isOn
        
        rowViewModel.cellPressed(tableView, atIndexPath: indexPath, withViewModel: viewModel)
    }
}

extension DetailsTableVC: UITextViewDelegate {

    func textViewDidChange(_ textView: UITextView) {
        tableView.beginUpdates()
        fixTextView(textView)
        tableView.endUpdates()
    }

    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == "Add some notes..." {
            textView.text = ""
        }
        return true
    }

    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        guard let idxPath = tableView.indexPath(for: textView) else { return true}
        guard let viewModel = self.viewModels[idxPath.section] else { return true }
        
        guard var rowViewModel = viewModel.viewModel(for: idxPath.row) as? TextViewRepresentable else {return true}
        rowViewModel.text = textView.text
        
        return true
    }
}

extension DetailsTableVC: DatePickerDelegate {
    func datePickerValueChanged(_ sender: UIDatePicker) {
        guard var idxPath = tableView.indexPath(for: sender) else { return }
        idxPath = IndexPath(row: idxPath.row - 1, section: idxPath.section)
        guard let viewModel = self.viewModels[idxPath.section] as? RemindOnDateViewModel else { return }

        print("viewModel displaydate = \(viewModel.displayDate)")
        print("sender.date = \(sender.date)")
        viewModel.displayDate = sender.date
        print("viewModel displaydate = \(viewModel.displayDate)")
        
        guard let cell = tableView.cellForRow(at: idxPath) as? DetailAlarmCell else { return }
        cell.alarmLabel.text = HelperClass.dateFormatterFull.string(from: viewModel.displayDate)
        cell.dateLabel.text = ""
    }
}

extension DetailsTableVC: HorizontalScrollerViewDelegate {
    
    func horizontalScrollerView(_ horizontalScrollerView: HorizontalScrollerView, didSelectViewAt index: Int) {
        
        let tagView = horizontalScrollerView.view(at: index) as! TagView

        guard let viewModel = self.viewModels[3] as? CategoriesAndTagsViewModel else {return}
        guard let rowViewModel = viewModel.viewModel(for: 2) as? DetailTagsPickerViewModel else { return }
        rowViewModel.didSelectTag(withTagView: tagView, at: index)

    }
}

