//
//  DetailTitleCellViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

protocol TextViewRepresentable {
    var text: String {get set}
}

struct DetailTitleCellViewModel {
    
    let task: Task
    
    var text: String {
        get {
            var text: String = ""
            if let title = task.title { text = title }
            return text
        }
        set {
            print("in title")
            task.title = newValue
        }
    }
}

extension DetailTitleCellViewModel: RowViewModel {}

extension DetailTitleCellViewModel: TextViewRepresentable {}
