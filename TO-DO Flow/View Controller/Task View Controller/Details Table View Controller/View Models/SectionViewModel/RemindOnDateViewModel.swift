//
//  RemindOnDateViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import Foundation
import EventKit

class RemindOnDateViewModel {
    
    let task: Task
    
    init(task: Task) {
        self.task = task
    }
    
    var taskReminder: EKReminder? {
        guard task.hasAlarm == true else { return nil }
        guard let calendarIdentifier = task.calendarIdentifier else { return nil }
        guard let reminder = TimedReminderStore.shared.fetchReminderWithCalendarItemIdentifier(with: calendarIdentifier) as? EKReminder else {return nil}
        return reminder
    }
    
    fileprivate var date: Date = Date()
    var displayDate: Date {
        get {
            
            if let reminder = taskReminder {
                if let alarmDate = reminder.alarms?.first?.absoluteDate {
                    date = alarmDate
                }
            }
            
            return date
        }
        set {
            date = newValue
        }
    }

    
    var toggleReminderStatus: Bool {
        return task.hasAlarm
    }
    
    var cellIndexPath: IndexPath? = nil
    
    // Handle Date Picker
    var hasInlineCell: Bool {
        return self.cellIndexPath != nil
    }
    
    // Determines if the given indexPath points to a cell that contains UIDatePicker
    func rowHasCell(for index: Int) -> Bool {
        return self.hasInlineCell && self.cellIndexPath?.row == index
    }
   
}


extension RemindOnDateViewModel: SectionRepresentable {
    var numberOfRows: Int {
        var numberOfRows = 1
        
        if toggleReminderStatus && hasInlineCell {
            // Return 4 rows if the Date Picker is shown and ToggleReminder is On
            numberOfRows = 4
        }
        else if toggleReminderStatus && !hasInlineCell {
            // Return 3 rows if the Date Picker is hidden and ToggleReminder is On
            numberOfRows = 3
        }
        // Otherwise return 1 row
        return numberOfRows
    }
    
    var titleForHeader: String? {
        return nil
    }
    
    func cellIdentifier(for index: Int) -> String {
        var identifier: String
        
        if index == 0 {
            identifier = DetailRemindMeOnDateCell.identifier
        }
        else if index == 1 {
            identifier = DetailAlarmCell.identifier
        }
        else if rowHasCell(for: index) {
            identifier = DetailDatePickerCell.identifier
        }
        else {
            identifier = DetailFrequencyCell.identifier
        }
        return identifier
    }
    
    
    func viewModel(for index: Int) -> RowViewModel {
        if index == 0 {
            return DetailRemidOnDateCellViewModel(task: task)
        }
        else if index == 1 {
            return DetailAlarmCellViewModel(task: task, displayDate: displayDate)
        }
        else if rowHasCell(for: index) {
            return DetailDatePickerCellViewModel(displayDate: displayDate)
        }
        else {
            return DetailFrequencyCellViewModel(task: task)
        }
    }
}
