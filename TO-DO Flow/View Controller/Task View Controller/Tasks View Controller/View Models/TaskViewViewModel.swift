//
//  TaskViewViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/22/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import CoreData


struct TaskViewViewModel {

    // MARK: - Property
    let fetchedResultsController: NSFetchedResultsController<Task>
    
    init(fetchedResultsController: NSFetchedResultsController<Task>) {
        self.fetchedResultsController = fetchedResultsController
        fetchTasks()
    }
    
    
    // MARK: - Helper Methods
    
    private func fetchTasks() {
        do {
            try self.fetchedResultsController.performFetch()
            
        } catch {
            print("Unable to Perform Fetch Request")
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    var hasTasks: Bool {
        guard let fetchedObjects = fetchedResultsController.fetchedObjects else { return false}
        return fetchedObjects.count > 0
    }
    
    var numberOfSections: Int {
        var numberOfSections = 1
        if let section = fetchedResultsController.sections {
            numberOfSections = section.count
        }
        return numberOfSections
    }
    
    
    
    // MARK: - methods
    func viewModel(for indexPath: IndexPath) -> TaskViewDataViewModel {
        let task = self.fetchedResultsController.object(at: indexPath)
        return TaskViewDataViewModel(task: task)
    }

    func titleForSection(for section: Int) -> String? {
        var titleSection: String?
        
        if let sectionInfo = fetchedResultsController.sections?[section] {
            titleSection = sectionInfo.name
        }
        return titleSection
    }
    
    func numberOfRows(for section: Int) -> Int {
        var numberOfRows = 0
        
        if let section = fetchedResultsController.sections?[section] {
            numberOfRows = section.numberOfObjects
        }
        return numberOfRows
    }
}
