//
//  TaskViewDataViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/22/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import EventKit
import UIKit


struct TaskViewDataViewModel {
    
    let task: Task
//
//    init(task: Task) {
//        self.task = task
//
//    }
    
    var title: String {
        return task.title!
    }
    
    var isFinished: Bool {
        return task.isFinished
    }
    
    // Fetch Reminder matching with Task
    private var reminder: EKReminder? {
        guard task.hasAlarm == true else { return nil }
        guard let calendarIdentifier = task.calendarIdentifier else { return nil }
        guard let reminder = TimedReminderStore.shared.fetchReminderWithCalendarItemIdentifier(with: calendarIdentifier) as? EKReminder else {return nil}
        return reminder
    }
    
    var dateAndFrequency: String? {
        guard let reminder = reminder else {return nil}
        
        var dateAndFrequency: String
        let gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
        
        // Fetch the date by which the reminder should be completed
        let date = gregorian.date(from: reminder.dueDateComponents!)
        let formattedDateString = HelperClass.dateFormatterShort.string(from: date!)
        var frequency: String = ""
        
        // If the reminder is a recurring one, only show its first recurrence rule
        if (reminder.hasRecurrenceRules) {
            
            // Fetch all recurrence rules associated with this reminder
            let recurrencesRules = reminder.recurrenceRules!
            let rule = recurrencesRules.first!
            frequency = rule.nameMatchingRecurrenceRuleWithFrequency( rule.frequency,
                                                                      interval: rule.interval)
        }
        
        dateAndFrequency = (reminder.hasRecurrenceRules) ? "\(formattedDateString), \(frequency)" : formattedDateString
        return dateAndFrequency
    }
    
    var taskTagsView: UIView {
        guard let tags = task.tag?.allObjects as? [Tag] else { return UIView() }
        
        let taskTagsView = UIView()
        
        var tagsColor: [UIView] = []
        
        for tag in tags {
            let tagView = TagViewInTask(frame: CGRect(x: 0, y: 0, width: 12, height: 12), tagColor: tag.colorName!)
            tagsColor.append(tagView)
        }
        
        var xValued: CGFloat = 0
        
        for view in tagsColor {
            view.frame = CGRect(x: xValued,
                                y: 0,
                                width: 12,
                                height: 12)
            taskTagsView.addSubview(view)
            xValued += 6
        }
        
        return taskTagsView
    }
    
    
}

