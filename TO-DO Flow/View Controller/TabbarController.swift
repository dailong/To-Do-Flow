//
//  TabbarController.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/3/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import CoreData

class TabbarController: UITabBarController {

    let notificationCenter = NotificationCenter.default
    let mainQueue = OperationQueue.main
    
    let coreDataManager = CoreDataManager(modelName: "Tasks")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passData()
    
        // Register for TimedReminderStore notifications
        let _ = notificationCenter.addObserver(forName: Notification.Name.AccessGrantedNotification,
                                               object: TimedReminderStore.shared,
                                               queue: mainQueue
        ) {[weak self] note in
            self?.handleAccessGrantedNotification(note)
        }
        
        let _ = notificationCenter.addObserver(forName: Notification.Name.RefreshDataNotification,
                                                         object: TimedReminderStore.shared,
                                                         queue: mainQueue) { [weak self] note in
                                                            self?.handleAccessGrantedNotification(note)
        }
        
        let _ = notificationCenter.addObserver(forName: Notification.Name.FailureNotification,
                                         object: TimedReminderStore.shared,
                                         queue: mainQueue
        ) {[weak self] note in
            self?.handleFailureNotification(note)
        }
        TimedReminderStore.shared.checkEventStoreAuthorizationStatus()
    }
    
    
    private lazy var fetchedResultsTaskController: NSFetchedResultsController<Task> = {
        // Create Fetch Request
        let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()
        
        let predicate = NSPredicate(format: "isFinished = 0")
        // Configure Fetch Request
        //      fetchRequest.predicate = predicate
        
        fetchRequest.sortDescriptors = [
            NSSortDescriptor(key: #keyPath(Task.updatedAt), ascending: false)
        ]
        
        
        // Create Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        
        // Configure Fetched Results Controller
      //  fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    // pass data to task VC
    func passData() {
        let allTabs = self.viewControllers
        let navigationVC = allTabs?.first as! UINavigationController
        let tasksVC = navigationVC.topViewController as! TasksVC
        
        loadData()
        
        tasksVC.viewModel = TaskViewViewModel(fetchedResultsController: fetchedResultsTaskController)
        
    }
    
    private func loadData() {
        
        let defaults = UserDefaults.standard
        
        let isLoaded = defaults.bool(forKey: "isLoaded")
        
        if !isLoaded {
            print("loading")
            
            
            // Category
            let grocery = Category(context: coreDataManager.managedObjectContext)
            grocery.name = "grocery"
            
            // Tag
            let red = Tag(context: coreDataManager.managedObjectContext)
            red.name = "red"
            red.colorName = "red"
            let green = Tag(context: coreDataManager.managedObjectContext)
            green.name = "green"
            green.colorName = "green"
            
            let brown = Tag(context: coreDataManager.managedObjectContext)
            brown.colorName = "brown"
            let cyan = Tag(context: coreDataManager.managedObjectContext)
            cyan.colorName = "cyan"
            let magenta = Tag(context: coreDataManager.managedObjectContext)
            magenta.colorName = "magenta"
            let orange = Tag(context: coreDataManager.managedObjectContext)
            orange.colorName = "orange"
            let purple = Tag(context: coreDataManager.managedObjectContext)
            purple.colorName = "purple"
            let yellow = Tag(context: coreDataManager.managedObjectContext)
            yellow.colorName = "yellow"
            
            //repeat
            let orange1 = Tag(context: coreDataManager.managedObjectContext)
            orange1.colorName = "orange1"
            let purple1 = Tag(context: coreDataManager.managedObjectContext)
            purple1.colorName = "purple1"
            let yellow1 = Tag(context: coreDataManager.managedObjectContext)
            yellow1.colorName = "yellow1"
            
            do {
                try coreDataManager.managedObjectContext.save()
            }
            catch {
                fatalError("Can't save data")
            }
            
            defaults.set(true, forKey: "isLoaded")
        }
        else {
            print("Data was loaded before!")
        }
    }
    
    
    //MARK: - Handle Access Granted Notification
    
    // Handle the RSAccessGrantedNotification notification
    private func handleAccessGrantedNotification(_ notification: Notification) {
        self.accessGrantedForReminders()
    }
    
    // Access was granted to Reminders. Fetch past-due, pending, and completed reminders
    private func accessGrantedForReminders() {
        TimedReminderStore.shared.fetchAllReminders()
      //  TimedReminderStore.shared.fetchUpcommingReminderWithDueDate(HelperClass.dateByAddingDays(7))
    }
    
    //MARK: - Handle Failure Notification
    
    // Handle the RSFailureNotification notification.
    // An error has occured. Display an alert with the error message.
    private func handleFailureNotification(_ notification: Notification) {
        let myNotification = notification.object as! TimedReminderStore?
        
        let alert = HelperClass.alert(title: NSLocalizedString("Status", comment: ""),
                                      message: myNotification?.errorMessage ?? "")
        self.present(alert, animated: true, completion: nil)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        // Unregister for TimedTabBarController notifications
        notificationCenter.removeObserver(self,
                                          name: Notification.Name.AccessGrantedNotification,
                                          object: nil)
        
        notificationCenter.removeObserver(self,
                                          name: Notification.Name.RefreshDataNotification,
                                          object: nil)
        
        notificationCenter.removeObserver(self,
                                          name: Notification.Name.FailureNotification,
                                          object: nil)
    }
}
